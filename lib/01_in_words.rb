class Fixnum
  DICTIONARY = {
    1 =>
      ['', 'one', 'two', 'three', 'four', 'five', 'six', 'seven',
       'eight', 'nine'],
    2 =>
      ['ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen',
       'sixteen', 'seventeen', 'eighteen', 'nineteen'],
    3 =>
      ['zero', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty',
       'seventy', 'eighty', 'ninety']
  }

  SUFFIXES = ['', 'thousand', 'million', 'billion',
              'trillion']

  def in_words
    return 'zero' if self == 0

    #chunk string into groups of 3
    digits = self.to_s.split(/\D/).join('') #digits is a string
    chunks = []
    while digits.length > 3
      chunks.unshift(digits.slice!(-3..-1))
    end
    chunks.unshift(digits)

    #convert each chunk and string them together
    i = 1
    while i <= chunks.length
      chunks[-i] = convert_chunk(chunks[-i]).strip + ' '
      chunks[-i] += SUFFIXES[i - 1] + ' ' if chunks[-i] != ' '
      chunks[-i] = chunks[-i].strip if chunks[-i] == ' '
      i += 1
    end

    chunks.join('').strip
  end

  def convert_chunk(chunk)
    str = ''

    i = 1
    while i <= chunk.length
      if i == 1
        str << DICTIONARY[i][chunk[-i].to_i]
      elsif i == 2
        if chunk[-i] == '1'
          str = DICTIONARY[i][chunk[-1].to_i]
        elsif chunk[-i] != '0'
          str = (DICTIONARY[3][chunk[-i].to_i] + ' ' + str)
        end
      else
        break if chunk[-i] == '0'
        str = (DICTIONARY[1][chunk[-i].to_i] + ' hundred ') + str
      end
      # str << DICTIONARY[i][chunk[-i].to_i]
      i += 1
    end

    str
  end

end
